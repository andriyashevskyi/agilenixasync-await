# AgileNix Async/Await

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.

You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).
##Рабочий пример

[https://agilenix-4a6ca.firebaseapp.com/](https://agilenix-4a6ca.firebaseapp.com/)

##Контакты для связи
[Telegram](https://t.me/a_andriyashevskyi)

[Почта](mailto:a.andriyashevskyi@gmail.com)

##Install and Start

npm install

npm run start

##Структура

./src/fetchCrypt.js - запрос для получения данных 

./src/sort/sort.js  - сортировка массива данных по порядку

./src/actions/ - экшн генераторы

./src/reducers/ - редьюсеры

./src/store/ - стор

./src/components/ - компоненты: строка таблицы и прелоадер

./src/containers/  - таблица