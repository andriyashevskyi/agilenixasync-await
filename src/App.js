import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';
import { gettingCrypto } from "./actions/crypto";
import Table from "./containers/Table";
import Preloader from "./components/Preloader";
import ErrorMSG from "./components/ErrorMSG";

class App extends Component {
  render() {
    const {getCrypt, request} = this.props;
    return (
        <React.Fragment>
          <div className="App">
            <header className="App-header">
              <h1 className="App-title">Cryptocurrency</h1>
            </header>
            <div className="container">
              {request.loaded ? <div className="table-container"><Table/></div> :
                  <button onClick={getCrypt}>Get Table</button>}
            </div>

          </div>
          {request.loading ? <Preloader/> : null}
          {request.error ? <ErrorMSG/> : null}
        </React.Fragment>
    );
  }
}


const mapStateToProps = (store) => ( {
  request: store.request
} );

const mapDispatchToProps = (dispatch) => ( {
  getCrypt: () => dispatch(gettingCrypto())
} )

export default connect(mapStateToProps, mapDispatchToProps)(App);
