export const cryptoFetchRequested = () => ( {
  type: 'CRYPTO_FETCH_REQUESTED',
  status: {
    loading: true,
    error: false,
    loaded: false
  }
} );

export const cryptoFetchSucceeded = () => ( {
  type: 'CRYPTO_FETCH_SUCCEEDED',
  status: {
    loading: false,
    error: false,
    loaded: true
  }
} );

export const cryptoFetchFailed = () => ( {
  type: 'CRYPTO_FETCH_FAILED',
  status: {
    loading: false,
    error: true,
    loaded: false
  }
} );