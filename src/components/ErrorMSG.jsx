import React, { Component } from 'react';



class ErrorMSG extends Component {
  state = {
    close: false
  };

  close = () => {
    this.setState({
      close: true
    })
  };

  render() {
    const {close} = this.state;
    let children = close
        ? null
        : ( <div className="error-msg-container">
          <div className="error-msg">
            <h2>Oops, something went wrong...</h2>
            <p>Check your internet connection please</p>
            <button onClick={this.close}>Okay</button>
          </div>
        </div> );

    return ( children )
  }
}


export default ErrorMSG;