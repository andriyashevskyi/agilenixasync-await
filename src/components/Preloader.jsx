import React from 'react';

function Preloader() {
  return <div className="preloader"><img
      src="https://loading.io/spinners/blocks/lg.rotating-squares-preloader-gif.gif"/></div>;
}


export default Preloader;