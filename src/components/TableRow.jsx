import React from 'react';
import PropTypes from 'prop-types';


function TableRow({number, name, ImageUrl, BaseImageUrl, price}) {
  const url = `${BaseImageUrl}${ImageUrl}`;
  return (
      <React.Fragment>
        <tr>
          <td>{number}</td>
          <td>{name}</td>
          <td><img src={url} alt="logo"/></td>
          <td>{price}</td>
        </tr>
      </React.Fragment>
  )
}

TableRow.propTypes = {
  number: PropTypes.number.isRequired,
  name: PropTypes.string,
  ImageUrl: PropTypes.string,
  BaseImageUrl: PropTypes.string,
  price: PropTypes.number,
};
TableRow.defaultProps = {
  number: 1,
  name: 'BTC',
};

export default TableRow;