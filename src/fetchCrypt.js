import sortCryptWith from "./sort/sort";

export const getPromise = new Promise((resolve, reject) => {
  fetch('https://min-api.cryptocompare.com/data/all/coinlist')
      .then((response) => {
        if ( !response.ok ) {
          reject(response)
        }

        const body = response.json();
        resolve(body);

      })
});


/**
 * проверяем ответ от сервера
 * @param data
 * @returns {*}
 */
export const checkResponse = (data) => {
  const {Response: response, Message: msg, ErrorsSummary: errSum} = data;
  if ( response === 'Error' ) {
    throw  Error(`Oops, here bad request ${msg} ${errSum}`);
  }
  return ( data );
};
/***
 * сортируем полученный объект
 * @param data
 * @returns {Array}
 */
export const sortArr = (data) => {
  let cryptoArr = [];
  const {BaseImageUrl: imgUrl, Data: coins,} = data;

  for ( let key in coins ) {
    if ( coins.hasOwnProperty(key) ) {
      //Запрос на получение цены можно засунуть вот сюда в таком виде(но не совсем правильно и красиво),
      // но  сервер для некоторых значений будет отдавать ошибку, ибо будут слишком частые обращения к серверу
      // либо же сделать async/await
      //Ниже решение, через async/await, которое и используется

      // fetch(`https://min-api.cryptocompare.com/data/price?fsym=${coins[ key ].Name}&tsyms=USD`)
      //     .then((data) => {
      //       return data.json()
      //     })
      //     .then((data) => {
      //       console.log(data);
      //       coins[ key ].price = data[ "USD" ];
      //     });
      coins[ key ].baseImgUrl = imgUrl;

      cryptoArr.push(coins[ key ]);
    }
  }
  cryptoArr.sort(sortCryptWith);
  return cryptoArr;
};

export async function getPrice(arr) {
  let promises = [];

  function getP(item) {
    return fetch(`https://min-api.cryptocompare.com/data/price?fsym=${item.Name}&tsyms=USD`)
        .then((response) => {
          if ( !response.ok ) {
            throw  Error('we have a problem');
          }
          return response.json()
        })
        .then((data) => {
          return {...item, ...{'price': data.USD}}
        })
        .catch((err) => {
          console.log('in fetch crypt-->',err);
          throw new Error(err);
        })

  }

  arr.forEach((item) => {
    promises.push(getP(item));
  });

  let result = await Promise.all(promises);

  return result;


}
