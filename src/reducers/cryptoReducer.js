const cryptoReducer = (state = [], action) => {
  switch (action.type) {
    case 'ADD_CRYPTO':
      return [
        ...state,
        ...action.data
      ];
    default :
      return state;
  }
};

export default cryptoReducer;