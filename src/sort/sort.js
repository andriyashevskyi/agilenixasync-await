/**
 * функция сортировки массива криптовалют по параметру SortOrder
 * @param a
 * @param b
 * @returns {number}
 */
function sortCryptWith(a, b) {
  return a.SortOrder - b.SortOrder;
}

export default sortCryptWith;





