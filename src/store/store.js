import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import requestReducer from '../reducers/requestReducer';
import cryptoReducer from "../reducers/cryptoReducer";

const configureStore = () => {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(combineReducers({
    request: requestReducer,
    crypto: cryptoReducer
  }), composeEnhancers(applyMiddleware(thunk)));
  return store;
};

export default configureStore;